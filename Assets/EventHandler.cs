﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.EventSystems;

public class EventHandler : MonoBehaviour, IDragHandler{
    private RectTransform rectTransform;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 localPosition = GetLocalPosition(eventData.position);
        rectTransform.localPosition = localPosition;
    }

    private Vector2 GetLocalPosition(Vector2 screenPosition)
    {
        return transform.InverseTransformPoint(screenPosition);
    }

}