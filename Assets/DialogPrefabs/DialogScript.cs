﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogScript : MonoBehaviour {

    public Text infoText = null;

    public void setText(string _setText)
    {
        infoText.text = _setText;
    }
}
