﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class NekoScript : MonoBehaviour {

    enum ANIME_STATE
    {
        IDEL,
        WARK,
        EAT,
        SLEEP,
        MAX
    };

    public GameObject[] anime = new GameObject[(int)ANIME_STATE.MAX];
    ANIME_STATE currentAnimeState;

    enum SERIF_STATE
    {
        HAGUHAGU,
        NYAN,
        ZZZ,
        MAX
    };


    public DispItem[] serif = new DispItem[(int)SERIF_STATE.MAX];



    //private Animator animator;

    public GameObject[] movePathObject;
    private Component[] currentPath;

    public float moveTime = 3;

    private int currentPathNum = 1;
    private int nextPathNum = 1;

    private bool onHuton = false;
    private bool onNekobed = false;
    private bool isMove = false;
    public bool isDragging = false;

    private BoxCollider2D boxCollider = null;
    public Collider2D lastColider = null;

    public GameManager gameManager = null;

    public SoundManager soundManager = null;

    public bool escape()
    {
        if (!onHuton)
        {
            return false;
        }

        // ネコベットの場合は逃げない
        if (onNekobed)
        {
            return true;
        }

        //Debug.Log("escape");
        
        lastColider = null;
        isMove = false;
        onHuton = false;
        onNekobed = false;
        isDragging = false;
        NextMove();
        boxCollider.enabled = true;
        

        transform.rotation = Quaternion.Euler(0, 0, 0);
        gameManager.NekoCountDown();

        return true;
    }

    private void Awake()
    {
    }
    // Use this for initialization
    void Start () {
        
        currentPath = movePathObject[0].GetComponentsInChildren(typeof(Transform));
        boxCollider = (BoxCollider2D)gameObject.GetComponent(typeof(BoxCollider2D));
        NextMove();
    }

    void setAnime(ANIME_STATE state)
    {
        for (int i=0; i< anime.Length; i++)
        {
            anime[i].SetActive(false);
        }

        currentAnimeState = state;
        anime[(int)state].SetActive(true);
    }
    bool isAnime(ANIME_STATE state)
    {
        return (currentAnimeState == state);
    }

    void checkFlip(float _x)
    {
        SpriteRenderer render = anime[(int)currentAnimeState].GetComponent<SpriteRenderer>();

        render.flipX = !(_x > transform.position.x);
    }


    public void HaguHaguEnd()
    {
        //Debug.Log("Callback : "); // Callback : おわったよ
        NextMove();
    }
    

    public void NextMove()
    {
        if (isDragging == true)
        {
            return;
        }

        // 移動していない状態からの場合は近い場所を探す
        if (isMove == false)
        {
            int minIndex = 1;
            float minDist = Vector3.Distance(((Transform)currentPath[1]).position, transform.position);
            for (int i=2; i<currentPath.Length ;i++)
            {
                float dist = Vector3.Distance(((Transform)currentPath[i]).position, transform.position);
                if (dist < minDist)
                {
                    minDist = dist;
                    minIndex = i;
                }
            }
            nextPathNum = minIndex;
        }

        // ご飯がある場合
        if (gameManager.sakanaSetting)
        {
            nextPathNum = 8;

            if (currentPath[currentPathNum].name == "8_gohan")
            {
                setAnime(ANIME_STATE.EAT);
                iTween.Stop(gameObject);

                transform.rotation = Quaternion.Euler(0, 0, 0);
                serif[(int)SERIF_STATE.HAGUHAGU].setDisp(HaguHaguEnd);

                //Debug.Log("Eat!!");
                return;
            }
        }

        if(gameManager.state == GameManager.SCENE_STATE.TITLE)
        {
            if (currentPathNum == 4)
            {
                nextPathNum = 3;
            }
            else
            {
                nextPathNum = 4;
            }
        }

        currentPathNum = nextPathNum;

        float speed = 1;
        setAnime(ANIME_STATE.WARK);




        isMove = true;
        Transform tmp = (Transform)currentPath[currentPathNum];
        Hashtable hash = iTween.Hash("x", tmp.position.x + Random.Range(0f,1f), "y", tmp.position.y + Random.Range(0f, 1f));


        float nextDist = Vector3.Distance(((Transform)currentPath[currentPathNum]).position, transform.position);

        hash.Add("time", nextDist / speed); // メソッドがあるオブジェクトを指定
        hash.Add("oncompletetarget", gameObject); // メソッドがあるオブジェクトを指定
        hash.Add("oncomplete", "NextMove"); // 実行するタイミング、実行するメソッド名
        iTween.MoveTo(gameObject, hash);

        checkFlip(tmp.position.x);

        // 次の移動位置を決める
        nextPathNum += Random.Range(-3,3);

        if (nextPathNum >= currentPath.Length)
        {
            nextPathNum = 1; // 1始まり
        }
        else if (nextPathNum < 0)
        {
            nextPathNum = (currentPath.Length-1) + nextPathNum;
        }
    }

    public void OnDrag(BaseEventData data)
    {
        isDragging = true;
        isMove = false;
        iTween.Stop(gameObject);

        var eventData = (PointerEventData)data;


        transform.position = eventData.pointerCurrentRaycast.worldPosition;
        
    }

    public void OnEndDrag(BaseEventData data)
    {
        //Debug.Log("OnEndDrag");
        isDragging = false;


        // 寝ている時以外は移動に戻る
        if (!isAnime(ANIME_STATE.SLEEP))
        {
            NextMove();
            soundManager.Play(SoundManager.SE.OKENAI);
        }
        else
        {
            if(lastColider != null)
            {
                //Debug.Log("OnEndDrag HitObj:" + lastColider.name);
                if (lastColider.name == "huton")
                {
                    if(Random.Range(0,2) == 0)
                    {
                        transform.Rotate(new Vector3(0f, 0f, -90));
                    }
                    onHuton = true;
                    boxCollider.enabled = false;
                    gameManager.NekoCountUp(false);

                    soundManager.Play(SoundManager.SE.OITATOKI);
                }
                else if (lastColider.name == "nekobed")
                {
                    if (gameManager.nekoBedCount < 15)
                    {
                        onNekobed = true;
                        boxCollider.enabled = false;
                        gameManager.NekoCountUp(true);

                        soundManager.Play(SoundManager.SE.OITATOKI);
                    }
                    else
                    {  
                        // ネコベッドがいっぱいだった場合
                        NextMove();

                        soundManager.Play(SoundManager.SE.OKENAI);
                    }
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if(onHuton == true)
        {
            return;
        }
        if (onNekobed == true)
        {
            return;
        }
        if (isDragging == false)
        {
            return;
        }

        if (collider.name == "huton")
        {
            //Debug.Log("OnTriggerEnter2D:" + collider.name);
            setAnime(ANIME_STATE.IDEL);
        }
        else if (collider.name == "nekobed")
        {
            //Debug.Log("OnTriggerEnter2D:" + collider.name);
            setAnime(ANIME_STATE.IDEL);
        }

        lastColider = null;
    }


    void OnHitSleepObject(Collider2D collider)
    {
        if (isDragging == false)
        {
            return;
        }
        if (collider.name == "BackGround")
        {
            return;
        }
        if (collider.tag == "neko")
        {
            return;
        }

        lastColider = collider;

        if (lastColider.name == "huton")
        {
            //Debug.Log("OnTriggerEnter2D:" + lastColider.name);
            setAnime(ANIME_STATE.SLEEP);
        }
        else if (lastColider.name == "nekobed")
        {
            //Debug.Log("OnTriggerEnter2D:" + lastColider.name);
            setAnime(ANIME_STATE.SLEEP);
        }
    }

    void OnTriggerStay2D(Collider2D collider)
    {
        OnHitSleepObject(collider);
    }
    //オブジェクトが衝突したとき  // ぶつかった瞬間に呼び出される
    void OnTriggerEnter2D(Collider2D collider)
    {
        OnHitSleepObject(collider);
    }

        //public void OnDrag(BaseEventData data)
        ////public void OnDrag()
        //{
        //    iTween.Stop(gameObject);
        //    //Debug.Log("drag");

        //    var eventData = (PointerEventData)data;
        //    //Debug.Log(eventData.position);

        //    //Debug.Log(eventData.pointerCurrentRaycast.worldPosition);
        //    transform.position = eventData.pointerCurrentRaycast.worldPosition;
        //    //Vector3 worldPos;
        //    //var ray = RectTransformUtility.ScreenPointToRay(Camera.main, eventData.position);
        //    //RaycastHit hit;
        //    //if (Physics.Raycast(ray, out hit))
        //    //{
        //    //    transform.position = hit.point + hit.normal * 0.5f;
        //    //}


        //    //localPosition = GetLocalPosition(eventData.position);

        //    //Debug.Log(localPosition);
        //    //rectTransform.localPosition = localPosition;

        //    //Vector3 pos = transform.position;
        //    //pos.x = localPosition.x;
        //    //pos.y = localPosition.y;
        //    //transform.position = pos;
        //}


        //private Vector3 GetWorldPositionFromRectPosition(RectTransform rect)
        //{
        //    //UI座標からスクリーン座標に変換
        //    Vector2 screenPos = RectTransformUtility.WorldToScreenPoint(Camera.main, rect.position);

        //    //ワールド座標
        //    Vector3 result = Vector3.zero;

        //    //スクリーン座標→ワールド座標に変換
        //    RectTransformUtility.ScreenPointToWorldPointInRectangle(rect, screenPos, Camera.main, out result);

        //    return result;
        //}

        // Update is called once per frame
        void Update () {
    }
}
