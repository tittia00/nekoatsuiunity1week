//-------------------------------------------------------------------------------------
// Loop !! Free Trial Version
// @2018 marching dream
// Version 1.0
//-------------------------------------------------------------------------------------

Loop! Free Trial Version is free trial version which music loops of various music genres such as Rock, Pops, Electronica, Ambient in.
4 music loops are included in this asset (file format is mp3) and each 4 music loops have 3 patterns(Full,Intro,Main).

- Rock 01
(Full 3:36 + Intro 0:16 , Main 0:32)
- Pops 01
(Full 2:24 + Intro 0:16 , Main 0:16)
- Electronic 01
(Full 2:47 + Intro 0:18 , Main 0:18)
- Ambient 01
(Full 3:39 + Intro 0:27 , Main 0:54)

Please check other loops & sounds in official site or official SoundCloud if you like this asset.



Official Site
http://marchingdream.blog.jp

Official SoundCloud
https://soundcloud.com/marchingdream


Version 1.0 
- Fist Release