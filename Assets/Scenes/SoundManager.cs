﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    public AudioSource[] audioSe;

    public enum SE
    {
        OITATOKI,
        SAKANA,
        CLEAR,
        OKENAI,
        SAKANAOKU,
        CLICK,
        AIRCON,
    };

    // Use this for initialization
    public void Play (SE playItem) {
        audioSe[(int)playItem].Play();

    }
}
