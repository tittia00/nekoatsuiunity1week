﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NekoManager : MonoBehaviour {

    public GameManager gameManager = null;
    public SoundManager soundManager = null;
    public GameObject[] movePathObject;

    public List<NekoScript> nekos = new List<NekoScript>();

    public GameObject[] nekosPrefabs = null;

    // Use this for initialization
    void Start()
    {
    }


    public void createNekos(int numberOfObjects)
    {
        float radius = 10f;
        for (int i = 0; i < numberOfObjects; i++) {
            float angle = i * Mathf.PI * 2 / numberOfObjects;
            Vector3 pos = new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), 0) * radius;

            int index = 0;
            if (Random.Range(0,100)<20)
            {
                index = 1;
            }
            GameObject newNeko = Instantiate(nekosPrefabs[index], pos, Quaternion.identity);

            NekoScript nekoScript = newNeko.GetComponent(typeof(NekoScript)) as NekoScript;
            nekoScript.gameManager = gameManager;
            nekoScript.soundManager = soundManager;
            nekoScript.movePathObject = movePathObject;

            nekos.Add(nekoScript);
        }
    }

    public void escapeNum(int i)
    {
        foreach (NekoScript neko in nekos)
        {
            if (neko.escape())
            {
                i--;
                if (i<=0)
                {
                    break;
                }
            }
        }
    }
    public void allEscape()
    {
        foreach (NekoScript neko in nekos)
        {
            neko.escape();
        }
    }

    public void allReset()
    {
        foreach (NekoScript neko in nekos)
        {
            Destroy(neko.gameObject);
        }

        for (int i=0; i < nekos.Count ;i++)
        {
            nekos.RemoveAt(i);
        }
        nekos.Clear();
    }

}
