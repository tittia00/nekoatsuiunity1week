﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SakanaScript : MonoBehaviour {

    public GameManager gameManager = null;
    public Transform gohanOkiba;
    private BoxCollider2D boxCollider = null;
    
    public GameObject popPoint;
    private Component[] currentPoints;

    private bool sakanaEatEnd = false;

    public SoundManager soundManager = null;

    // Use this for initialization
    void Start () {
        boxCollider = (BoxCollider2D)gameObject.GetComponent(typeof(BoxCollider2D));
        currentPoints = popPoint.GetComponentsInChildren(typeof(Transform));

        SakanaEnd();
        //Reset();
    }
	
	// Update is called once per frame
	void Update () {

    }

    private bool onOkiba = false;

    public void OnDrag(BaseEventData data)
    {
        if (onOkiba)
        {
            return;
        }
        var eventData = (PointerEventData)data;
        transform.position = eventData.pointerCurrentRaycast.worldPosition;

    }


    public void ResetTitle()
    {
        gameObject.SetActive(true);
        transform.position = ((Transform)currentPoints[2]).position;
        onOkiba = false;
        boxCollider.enabled = true;

        sakanaEatEnd = false;

        soundManager.Play(SoundManager.SE.SAKANA);
    }

    public void Reset(bool forceReset)
    {
        if (sakanaEatEnd == true || forceReset)
        {
            gameObject.SetActive(true);
            transform.position = ((Transform)currentPoints[Random.Range(0, currentPoints.Length)]).position;
            onOkiba = false;
            boxCollider.enabled = true;

            sakanaEatEnd = false;

            soundManager.Play(SoundManager.SE.SAKANA);
        }
    }

    public void SakanaEnd()
    {
        sakanaEatEnd = true;
        gameObject.SetActive(false);
    }
        //オブジェクトが衝突したとき  // ぶつかった瞬間に呼び出される
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.name == "gohan")
        {
            onOkiba = true;
            transform.position = gohanOkiba.position;
            boxCollider.enabled = false;
            gameManager.setSakana(SakanaEnd);

            soundManager.Play(SoundManager.SE.SAKANAOKU);
        }
    }
}
