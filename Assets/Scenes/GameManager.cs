﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class GameManager : MonoBehaviour {

    [DllImport("__Internal")]
    private static extern void Hello();

    [DllImport("__Internal")]
    private static extern void OpenWindow(string str);

    public NekoManager nekoManager = null;
    public DialogScript nekoCounter;
    public DialogScript comboCounter;
    public DialogScript nekoTotalCounter;
    public DialogScript TimeDisp;
    public DialogScript SakanaTimer;
    public DialogScript resultText;
    public Text timeSettingText;

    public GameObject[] titleDispObjects;

    public GameObject[] resultDispObjects;
    public GameObject[] gameDispObjects;
    public DialogScript ResultDialog;

    public DispItem aircon = null;

    private int nekoCount = 0;
    public int nekoBedCount = 0;
    private int ComboCounter = 0;
    private int ComboMaxCounter = 0;
    private int nekoTotalCount = 0;
    private int SakanaCounter = 0;

    private int Point = 0;

    private float startTime;
    private static float MAX_TIME = 30;
    //private static float MAX_TIME = 5;
    private float nowTime;


    private float startSakanaTime;
    private static float MAX_SAKANA_TIME = 10;
    private float nowSakanaTime;

    public enum SCENE_STATE
    {
        TITLE,
        //TUTORIAL,
        GAME,
        RESULT,
    };

    public SCENE_STATE state = SCENE_STATE.TITLE;

    public bool sakanaSetting = false;

    public SakanaScript sakana = null;
    public SakanaScript sakana2 = null;

    public SoundManager soundManager = null;

    private int[][] NEKO_ADD_PARAMES = new int[][]{
            new int[]{ 0,5,0},
            new int[]{ 5,5,0},
            new int[]{10,3,0},
            new int[]{15,3,0},
            new int[]{20,3,0},
            new int[]{25,3,0},
            new int[]{30,3,0},
            new int[]{35,3,0},
            new int[]{40,3,0},
            new int[]{45,3,0},
            new int[]{50,3,0},
            new int[]{55,3,0},
 
            new int[]{60,3,0},
            new int[]{65,3,0},
            new int[]{70,3,0},
            new int[]{75,3,0},
            new int[]{80,3,0},
            new int[]{85,3,0},

            new int[]{90,10,0},
            new int[]{95,10,0},
            new int[]{100,10,0},
            new int[]{105,10,0},
            new int[]{110,10,0},
            new int[]{115,10,0},
        };


	// Use this for initialization
	void Start () {
        TitleStart();
    }

    public void TitleStart()
    {
        soundManager.Play(SoundManager.SE.CLICK);
        sakana.ResetTitle();
        timeSettingText.text = MAX_TIME + "秒";
        for (int i = 0; i < resultDispObjects.Length; i++)
        {
            resultDispObjects[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < gameDispObjects.Length; i++)
        {
            gameDispObjects[i].gameObject.SetActive(false);
        }

        for (int i=0; i< titleDispObjects.Length; i++)
        {
            titleDispObjects[i].gameObject.SetActive(true);
        }
        state = SCENE_STATE.TITLE;
        nekoManager.createNekos(1);
    }

    public void SettingTime()
    {
        if (MAX_TIME == 120)
        {
            MAX_TIME = 30;
        }
        else if (MAX_TIME == 30)
        {
            MAX_TIME = 40;
        }
        else if (MAX_TIME == 40)
        {
            MAX_TIME = 60;
        }
        else if (MAX_TIME == 60)
        {
            MAX_TIME = 80;
        }
        else if (MAX_TIME == 80)
        {
            MAX_TIME = 120;
        }
        timeSettingText.text = MAX_TIME + "秒";
    }

    public void GameStart()
    {
        soundManager.Play(SoundManager.SE.CLICK);
        for (int i = 0; i < titleDispObjects.Length; i++)
        {
            titleDispObjects[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < gameDispObjects.Length; i++)
        {
            gameDispObjects[i].gameObject.SetActive(true);
        }


        for (int i = 0; i < NEKO_ADD_PARAMES.Length; i++)
        {
            NEKO_ADD_PARAMES[i][2] = 0;
        }

        // タイトルで作成したオブジェクトの削除
        nekoManager.allReset();
        sakana.SakanaEnd();
        sakana2.SakanaEnd();

        nekoCount = 0;
        nekoBedCount = 0;
        ComboCounter = 0;
        ComboMaxCounter = 0;
        SakanaCounter = 0;
        Point = 0;
        nekoTotalCount = 0;
        nekoCounter.setText("" + nekoCount);
        comboCounter.setText("" + ComboCounter);
        nekoTotalCounter.setText("" + nekoTotalCount);

        comboCounter.gameObject.SetActive(false);

        startSakanaTime = 0;
        nowSakanaTime = 0;
        SakanaTimer.setText("-");

        state = SCENE_STATE.GAME;
        startTime = Time.time;
    }

    public void UseAsset()
    {
        soundManager.Play(SoundManager.SE.CLICK);
        OpenWindow("http://tiatia.bambina.jp/game/NekoAtsuiUnity1Week/asset.html");
    }
    public void Twitter()
    {
        soundManager.Play(SoundManager.SE.CLICK);
        //Hello();
        OpenWindow("http://twitter.com/intent/tweet?text=ねこねかせで"+Point+ "ねかせポイントゲットした！ https://unityroom.com/games/nekonekase");
    }

    public void Ranking()
    {
        soundManager.Play(SoundManager.SE.CLICK);
        naichilab.RankingLoader.Instance.SendScoreAndShowRanking(Point);
    }

    public void ResultStart()
    {
        /*
ふとんねこ
ねこべっとねこ
ねかせたねこ
さいだいこんぼ
さかなかいすう
ーーーーーーーー
ごうけい

        nekoCount = 0;
        nekoBedCount = 0;
        ComboCounter = 0;
        nekoTotalCount = 0;
*/
        TimeDisp.setText("-");

        Point = 0;
        Point += nekoCount * 100;
        Point += nekoBedCount * 200;
        Point += nekoTotalCount * 100;
        Point += ComboMaxCounter * 1000;
        Point += SakanaCounter * 500;

        resultText.setText(
            "+ "+ nekoCount + " x " + 100 + "\n" +
            "+ " + nekoBedCount + " x " + 200 + "\n" +
            "+ " + nekoTotalCount + " x " + 100 + "\n" +
            "+ " + ComboMaxCounter + " x " + 1000 + "\n" +
            "+ " + SakanaCounter + " x " + 500 + "\n" +
            "ーーーーーーー\n" +
            ""+ Point);


        for (int i = 0; i < resultDispObjects.Length; i++)
        {
            resultDispObjects[i].gameObject.SetActive(true);
        }


        soundManager.Play(SoundManager.SE.CLEAR);


        Ranking();
        state = SCENE_STATE.RESULT;
    }

    public delegate void onCompleteSakana();
    private onCompleteSakana completeSakana;

    private onCompleteSakana completeSakana2;
    public void setSakana(onCompleteSakana _onComplete)
    {
        startSakanaTime = Time.time;
        sakanaSetting = true;

        SakanaCounter++;

        if (completeSakana == null)
        {
            completeSakana = _onComplete;
        }
        else if (completeSakana2 == null)
        {
            completeSakana2 = _onComplete;
        }
    }
	
	// Update is called once per frame
	void Update () {

        if (state != SCENE_STATE.GAME)
        {

            return;
        }

        nowTime = (Time.time - startTime);
        TimeDisp.setText("" + ((int)(MAX_TIME - nowTime)));

        if ((MAX_TIME - nowTime) <0)
        {
            ResultStart();

            return;
        }

        CheckNekoAddTime();

        if (sakanaSetting)
        {
            nowSakanaTime = (Time.time - startSakanaTime);

            SakanaTimer.setText("" + ((int)(MAX_SAKANA_TIME - nowSakanaTime)));

            if (nowSakanaTime>= MAX_SAKANA_TIME)
            {
                sakanaSetting = false;
                if (completeSakana != null)
                {
                    completeSakana();
                    completeSakana = null;
                }
                if (completeSakana2 != null)
                {
                    completeSakana2();
                    completeSakana2 = null;
                }
            }
        }
        else
        {

            SakanaTimer.setText("-");
        }

    }

    private void CheckNekoAddTime()
    {
        for (int i=0; i<NEKO_ADD_PARAMES.Length ;i++)
        {

            if (NEKO_ADD_PARAMES[i][2] == 0 && nowTime > NEKO_ADD_PARAMES[i][0])
            {
                nekoManager.createNekos(NEKO_ADD_PARAMES[i][1]);
                NEKO_ADD_PARAMES[i][2] = 1;
            }
        }
    }

    private void CheckCombo()
    {
        if (ComboCounter>=3)
        {
            comboCounter.gameObject.SetActive(true);
        }
        else
        {
            comboCounter.gameObject.SetActive(false);
        }
    }

    public void NekoCountUp(bool nekobed)
    {
        nekoCount++;
        ComboCounter++;
        nekoTotalCount++;
        nekoCounter.setText("" + nekoCount);
        comboCounter.setText("" + ComboCounter);
        nekoTotalCounter.setText("" + nekoTotalCount);

        if (nekobed)
        {
            nekoBedCount++;
        }

        CheckAirConditioner();
        CheckCombo();
    }

    private int[][] AIRCON_PARAMES = new int[][]{
        new int[]{  1,  5, 2,  0},
        new int[]{  2,  5, 2,  0},
        new int[]{  3,  5, 2,  0},
        new int[]{  4,  5, 2, 30},
        new int[]{  5, 10, 5, 70},
        new int[]{  6, 10, 5, 70},
        new int[]{  7, 30, 5, 70},
        new int[]{  8, 30, 6, 70},
        new int[]{  9, 60, 6, 70},
        new int[]{ 10, 60, 7, 90},
        new int[]{ 11, 60, 7, 90},
        new int[]{ 12, 60, 8, 90},
        new int[]{ 13, 60, 8, 90},
        new int[]{ 14, 60, 8, 90},
        new int[]{ 15, 60, 8, 90},
        new int[]{ 16,100, 8, 90},
        new int[]{ 17,100, 8,  0},
    };
    /**
     * エアコンを使うとネコが逃げる
     **/
    private void CheckAirConditioner()
    {
        // 魚タイム中はコンボが途切れない
        if (sakanaSetting)
        {
            // 確率で追加魚を出す
            if (30 >= Random.Range(1, 100))
            {
                sakana2.Reset(true);
            }
            return;
        }

        // コンボの設定以上の場合は最後のデータを使いまわす
        if (AIRCON_PARAMES[AIRCON_PARAMES.Length - 1][0] < ComboCounter)
        {
            Debug.Log("over check");
            int lastIndex = AIRCON_PARAMES.Length - 1;
            // カウンターチェックは行わない
            {
                if (Random.Range(0, 100) < AIRCON_PARAMES[lastIndex][1])
                {
                    nekoManager.escapeNum(nekoCount / 10 + Random.Range(1, AIRCON_PARAMES[lastIndex][2]));
                    aircon.setDisp(null);
                    soundManager.Play(SoundManager.SE.AIRCON);

                    if (AIRCON_PARAMES[lastIndex][3] >= Random.Range(1, 100))
                    {
                        sakana.Reset(false);
                    }
                }
            }
        }
        // データがある場合のチェック
        else
        {
            for (int i = 0; i < AIRCON_PARAMES.Length; i++)
            {
                if (AIRCON_PARAMES[i][0] == ComboCounter)
                {
                    if (Random.Range(0, 100) < AIRCON_PARAMES[i][1])
                    {
                        nekoManager.escapeNum(nekoCount / 10 + Random.Range(0, AIRCON_PARAMES[i][2]));
                        aircon.setDisp(null);
                        soundManager.Play(SoundManager.SE.AIRCON);

                        if (AIRCON_PARAMES[i][3] >= Random.Range(1, 100))
                        {
                            sakana.Reset(false);
                        }
                    }
                }
            }
        }

    }

    public void NekoCountDown()
    {
        nekoCount--;

        // マックスコンボの更新
        if (ComboCounter > ComboMaxCounter)
        {
            ComboMaxCounter = ComboCounter;
        }

        ComboCounter = 0;
        nekoCounter.setText("" + nekoCount);
        comboCounter.setText("" + ComboCounter);
        CheckCombo();
    }

}
