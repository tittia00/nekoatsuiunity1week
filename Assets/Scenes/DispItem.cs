﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DispItem : MonoBehaviour {

    private float startTime;
    private static float MAX_TIME = 3;
    private float nowTime;


    public delegate void onCompleteHaguhagu();
    private onCompleteHaguhagu onComplete;

    // Use this for initialization
    public void setDisp(onCompleteHaguhagu _onComplete)
    {
        //Debug.Log("びょうが");
        gameObject.SetActive(true);
        startTime = Time.time;
        onComplete = _onComplete;
    }

    private void Start()
    {
        startTime = Time.time - MAX_TIME;
        gameObject.SetActive(false);
    }

    private void Update()
    {
        nowTime = (Time.time - startTime);
        if(MAX_TIME <= nowTime)
        {
            gameObject.SetActive(false);
            if(onComplete != null)
            {
                onComplete();
            }
        }
    }


}
